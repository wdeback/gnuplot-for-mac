#!/bin/bash

OUTFILE="../build/src/gnuplot"
APPNAME="Gnuplot"
DYLIBBUNDLER="${HOME}/Software/macdylibbundler-master/dylibbundler"

# Read command line argument (if DMG is present, create disk image) 
CREATE_DMG=0
if [ -z "$1" ]
  then
    echo "No DMG"
  else
    if [ $1 == "dmg" ] || [ $1 == "DMG" ]
	then
	    echo "Create DMG"
	    CREATE_DMG=1
	else
		echo "Unknown argument: " $1 
	fi
fi


##### Make Bundle structure

APPBUNDLE="${APPNAME}.app"
APPBUNDLECONTENTS="${APPBUNDLE}/Contents"
APPBUNDLEEXE="${APPBUNDLECONTENTS}/MacOS"
APPBUNDLERESOURCES="${APPBUNDLECONTENTS}/Resources"
APPBUNDLEICON="${APPBUNDLECONTENTS}/Resources"

rm -rf ${APPBUNDLE}
mkdir ${APPBUNDLE}
mkdir ${APPBUNDLE}/Contents
mkdir ${APPBUNDLE}/Contents/MacOS
mkdir ${APPBUNDLE}/Contents/Resources
cp macosx/Info.plist ${APPBUNDLECONTENTS}/
cp ${OUTFILE} ${APPBUNDLEEXE}/${APPNAME}


###### Use dylibbundler to collect and fix all external dylib dependencies
## See https://github.com/auriamg/macdylibbundler/
$DYLIBBUNDLER -of -b -x ${APPBUNDLEEXE}/${APPNAME} -d ${APPBUNDLERESOURCES} -p @executable_path/../Resources

cp macosx/${APPNAME}.icns ${APPBUNDLEICON}/

###### Create DMG
if [ $CREATE_DMG == 1 ]
then
	echo "Creating disk image"
	BUNDLENAME=${APPNAME}.dmg
	IMAGEDIR="/tmp/${APPNAME}.$$"
	mkdir ${IMAGEDIR}
	cp -RP ${APPNAME}.app ${IMAGEDIR}
	ln -s /Applications ${IMAGEDIR}/Applications
	#cp -RP Applications ${IMAGEDIR}

	# TODO: copy over additional files, if any
	hdiutil create -ov -srcfolder ${IMAGEDIR} -format UDBZ -volname ${APPNAME} ${BUNDLENAME}
	hdiutil internet-enable -yes ${BUNDLENAME}
	rm -rf $imagedir
	echo "${BUNDLENAME} created"
else
	echo "NOTE: No DMG created!" 
	echo
	echo "To create a Disk Image bundle (DMG), type:" 
	echo "./make_bundle.sh dmg"
fi

