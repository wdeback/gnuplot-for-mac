export QT_PATH=/opt/local/libexec/qt4/Library/Frameworks

export QT_LIBS="-F$QT_PATH -framework QtCore -framework QtGui -framework QtNetwork -framework QtSvg" 

# NOTE: CFLAGS usually look more like 
#     -framework QtCore, 
# but then it would need to be used as 
#     #include <QtCore/QtCore> 
# 
# Current use of headers requires the weird 
# -I$QT_PATH/lib/QtCore.framework/Headers 

export QT_CFLAGS="-F$QT_PATH -I$QT_PATH/QtCore.framework/Headers -I$QT_PATH/QtGui.framework/Headers -I$QT_PATH/QtNetwork.framework/Headers -I$QT_PATH/QtSvg.framework/Headers" 

echo $QT_LIBS
echo $QT_CFLAGS

export UIC="/opt/local/libexec/qt4/bin/uic"
export MOC="/opt/local/libexec/qt4/bin/moc"
export RCC="/opt/local/libexec/qt4/bin/rcc"

#export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/opt/local/libexec/qt4/lib/pkgconfig

./configure --with-readline=builtin --with-pdf 
## Qt4 gives compiler error: gcc: error: unrecognized command line option '-rdynamic' 
# --with-qt=qt4
## WxWidgets gives errors concerning "-std=c++11" but even including this as CFLAGS and CXXFlags in Makefile does 
not solve this issue.
## --with-wx=/opt/local/Library/Frameworks/wxWidgets.framework/Versions/wxWidgets/3.0/bin/
