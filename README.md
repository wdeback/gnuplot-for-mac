[Gnuplot](http://www.gnuplot.info/) is a versatile graphing tools with the ability
to export to a large number of file formats (e.g. png, pdf, svg) and screen terminals (e.g. x11, wxt, qt).

**Looking for Gnuplot v5 Mac Bundle? [Download Gnuplot.dmg here](https://gitlab.com/wdeback/gnuplot-for-mac/blob/master/BUNDLE/Gnuplot.dmg).**


However, Gnuplot does not come in a handy Bundle (.app or .dmg) for users of 
Mac OSX. Therefore, software tools that use Gnuplot as a visualization back-end such 
as [Octave](https://www.gnu.org/software/octave/) and [Morpheus](https://imc.zih.tu-dresden.de/wiki/morpheus)
need to either force their Mac users to [install gnuplot through MacPorts](https://www.macports.org/ports.php?by=name&substr=gnuplot) 
or pack their own Gnuplot bundles. 

Octave suggests users to install the [Gnuplot-4.2.5 bundle](http://www.miscdebris.net/upload/gnuplot-4.2.5-i386.dmg) 
from [here](http://www.miscdebris.net/upload). However, this packs an outdated
Gnuplot, is not actively supported and has a few issues on newer Macs (e.g. 
missing ´libfreetype´).

Therefore, we decided to generate our own Mac bundle of Gnuplot for use in Morpheus. 
The code is a copy from the [gnuplot-5.0.3](https://sourceforge.net/projects/gnuplot/files/gnuplot/5.0.3/) 
downloaded from SourceForge.

We only added some scripts (see the [BUNDLE](https://gitlab.com/wdeback/gnuplot-for-mac/tree/master/BUNDLE) folder) to:
- configure and build the sources 
- generate a mac bundle (app) and a disk image (dmg)

Note that the script is not generic and paths will need to be customized for each individual system.

.

To copy and update the dylibs that gnuplot depends, this script depends on a great 
little tool called [dylibbundler](https://github.com/auriamg/macdylibbundler/) on GitHub: https://github.com/auriamg/macdylibbundler/

**Looking for the resulting Gnuplot v5 Mac Bundle? [Download Gnuplot.dmg here](https://gitlab.com/wdeback/gnuplot-for-mac/blob/master/BUNDLE/Gnuplot.dmg).**

Not working for you? Please let us know using the Issue tracker. 